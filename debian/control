Source: debomatic
Section: devel
Priority: optional
Maintainer: Luca Falavigna <dktrkranz@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               python3,
               dh-python,
               python3-setuptools,
               python3-sphinx,
               latexmk,
               texlive-latex-base,
               texlive-latex-recommended,
               texlive-fonts-recommended,
               texlive-latex-extra,
               texlive-plain-generic,
               tex-gyre
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://debomatic.github.io
Vcs-Git: https://salsa.debian.org/python-team/packages/debomatic.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/debomatic

Package: debomatic
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         ${sphinxdoc:Depends},
         debootstrap | cdebootstrap | qemu-user-static,
         sbuild,
         schroot,
         python3-toposort
Recommends: python3-pyinotify,
            gpgv,
            python3-systemd
Suggests: autopkgtest,
          blhc,
          devscripts,
          lintian,
          piuparts,
          apt-utils,
          gnupg,
          debian-archive-keyring
Description: automatic build machine for Debian source packages
 Deb-o-Matic is an easy to use build machine for Debian source packages
 based on sbuild and schroot, written in Python.
 .
 It provides a simple tool to automate build of source packages with limited
 user interaction and a simple configuration. It has some useful features such
 as automatic update of chroots, automatic scan and selection of source
 packages to build and modules support.
 .
 It is meant to help developers to build their packages without worrying too
 much of compilation, since it will run in background and no user feedback
 is required during the whole process.
