# Helper for the debomatic tests in this directory.
set -Ceu

# Environment variables: AUTOPKGTEST_TMP HOME

debomatictest_version() {
    # 1: source package name
    # -> currently available version
    rmadison -u debian -a source -s unstable $1 \
        | cut -d\| -f2 \
        | xargs
}
debomatictest_rebuild() {
    # Get the source.
    local component=$(rmadison -u debian -a source -s unstable $pkg \
                          | cut -d\| -f3 \
                          | cut -d/ -f2 \
                          | sed s/unstable/main/ \
                          | xargs)
    local initial=${pkg%${pkg#?}}
    local dsc=${pkg}_$version.dsc
    dget -u http://deb.debian.org/debian/pool/$component/$initial/$pkg/$dsc

    # Rebuild it.
    cd $pkg-${version%%-*}
    debuild -S -d -uc -us
    cd ..

    # Add to local repository.
    local profiles="$HOME"/.dput.d/profiles

    mkdir -p "$profiles"
    cat >| "$profiles"/local.json << EOF
{
    "meta": "debomatic",
    "incoming": "$AUTOPKGTEST_TMP/incoming",
    "method": "local",
    "check-debs": {
        "skip": true
    }
}
EOF

    dput local ${pkg}_${version}_source.changes
}
debomatictest_create_conf() {
    if [ $hostarchitecture = None ]; then
        local crossbuild=False
    else
        local crossbuild=True
    fi

    sed /etc/debomatic/debomatic.conf \
        -e 's/^modules: True/modules: False/' \
        -e "s|^incoming: .*|incoming: $AUTOPKGTEST_TMP/incoming|" \
        -e 's|^loglevel: .*|loglevel: debug|' \
        -e "s|^crossbuild: .*|crossbuild: $crossbuild|" \
        -e "s|^hostarchitecture: .*|hostarchitecture: $hostarchitecture|" \
        > $conf

    grep -F 'modules: False'                      $conf
    grep -F "incoming: $AUTOPKGTEST_TMP/incoming" $conf
    grep -F 'loglevel: debug'                     $conf
    grep -F "crossbuild: $crossbuild"             $conf
    grep -F "hostarchitecture: $hostarchitecture" $conf
}
debomatictest_check_log() {
    # Check status in the log.  Each argument is also checked.
    local log=incoming/unstable/pool/${pkg}_$version/${pkg}_$version.buildlog
    local line

    cat $log

    for line in 'Status: successful' "$@"; do
        grep -F "$line" $log
    done
}
debomatictest_clean() {
    rm -fr /etc/schroot/chroot.d/unstable-*-debomatic-* \
           /etc/sbuild/chroot/unstable-*-debomatic
}

# Common initialization

# Files created by tests outside AUTOPKGTEST_TMP must be removed both
# before the test (in case another test run manually has just failed
# without cleaning) and after the test (in case the test is not run in
# a throwable chroot).
debomatictest_clean
trap debomatictest_clean EXIT

cd "$AUTOPKGTEST_TMP"
mkdir incoming

# Non-default values may be set *before* inclusion of this file.
hostarchitecture=${hostarchitecture:-None}
pkg=${pkg:-hello}

version=`debomatictest_version $pkg`
conf=debomatic.conf
